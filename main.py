import math
import pygame

WIDTH = 640
HEIGHT = 640

creep_path = [
    (7.5 * 64, 2.5 * 64),
    (7.5 * 64, 5.5 * 64),
    (2.5 * 64, 5.5 * 64),
    (2.5 * 64, 8.5 * 64),
    (10 * 64, 8.5 * 64),
]

class Gun(Actor):

    FIRE_DELAY = 1000

    def __init__(self, pos):
        super().__init__("towerdefense_tile250", pos)
        self.range = 140
        self.angle_offset = 270
        self.can_fire = True
        
    def update(self): 
        # self.angle += 1

        for creep in creeps:
            if self.distance_to(creep) <= self.range:
                if self.can_fire:
                    self.fire(creep)
                self.angle = self.angle_to(creep) + self.angle_offset
                return

    def draw(self):
        # screen.draw.circle(self.pos, int(self.range), (255, 0, 0))
        super().draw()

    def fire(self, target):
        actors.append(Projectile(self.pos, target))
        self.can_fire = False
        clock.schedule(self.reload, float(self.FIRE_DELAY) / 1000.0)

    def reload(self):
        self.can_fire = True

class Creep(Actor):
    def __init__(self, pos, health):
        super().__init__("towerdefense_tile245", pos)
        self.health = health
        self.path_index = 0
        self.speed = 1
        self.alive = True
    
    def update(self):
        global player_health

        target_pos = creep_path[self.path_index]

        delta_x = target_pos[0] - self.x
        delta_y = target_pos[1] - self.y

        distance = math.sqrt(delta_x**2 + delta_y**2)
        
        if distance <= self.speed:
            self.path_index += 1
            if self.path_index >= len(creep_path):
                self.alive = False
                player_health -= 1
            else:
                self.x = target_pos[0]
                self.y = target_pos[1]
        else:
            target_angle = math.atan2(delta_y, delta_x)
            self.angle = -math.degrees(target_angle)
            self.x += self.speed * math.cos(target_angle)
            self.y += self.speed * math.sin(target_angle)
    
    def damage(self, amount):
        global money
        self.health -= amount
        if self.alive and self.health < 0:
            self.alive = False
            money += 5


class Projectile(Actor):
    def __init__(self, pos, target):
        super().__init__("towerdefense_tile297", pos)
        self.target = target
        self.speed = 5
        self.damage = 20
        self.angle_offset = 90

    def update(self):
        delta_x = self.target.x - self.x
        delta_y = self.target.y - self.y
        target_angle = math.atan2(delta_y, delta_x)
        self.angle = -math.degrees(target_angle) + self.angle_offset
        self.x += self.speed * math.cos(target_angle)
        self.y += self.speed * math.sin(target_angle)
        if self.target.collidepoint(self.pos):
            self.target.damage(self.damage)
            self.alive = False

# grass_tile = Actor("towerdefense_tile024")
# sand_tile = Actor("towerdefense_tile050")
map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 0, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
gun_map = {}

guns = [
    # Gun((200, 200)),
    # Gun((400, 200)),
    # Gun((200, 400)),
    # Gun((400, 400))
]
creeps = [
    # Creep((100, 50)),
    # Creep((200, 50)),
    # Creep((300, 50))
]
actors = guns + creeps
creep_spawn_point = (0 * 64, 2.5 * 64)
money = 50
player_health = 20
level = 0

def draw():
    # screen.fill((0, 0, 0))
    for y in range(len(map)):
        row = map[y]
        for x in range(len(row)):
            tile = row[x]
            if tile == 1:
                screen.blit("towerdefense_tile024", (x * 64, y * 64))
            elif tile == 0:
                 screen.blit("towerdefense_tile050", (x * 64, y * 64))

    for actor in actors:
        actor.draw()

    screen.draw.text("Level: {}".format(level), (20, HEIGHT - 100))
    screen.draw.text("Health: {}".format(player_health), (20, HEIGHT - 75))
    screen.draw.text("Money: {}".format(money), (20, HEIGHT - 50))

def update():
    global actors
    global guns
    global creeps
    global level
    
    filtered_actors = []
    for actor in actors:
        if (not hasattr(actor, 'alive')) or actor.alive == True:
            filtered_actors.append(actor)

    guns = []
    creeps = []

    for actor in actors:
        if isinstance(actor, Gun):
            guns.append(actor)
        if isinstance(actor, Creep):
            creeps.append(actor)

    if len(creeps) == 0:
        level += 1
        spawn_creeps(level * 10, 1000)

    actors = filtered_actors

    for actor in actors:
        actor.update()

def on_mouse_down():
    global money
    pos = pygame.mouse.get_pos()
    x = math.floor(pos[0] / 64)
    y = math.floor(pos[1] / 64)

    key = "{},{}".format(x,y)
    is_valid_location = map[y][x] == 1 and gun_map.get(key, None) == None
    if is_valid_location and money >= 20:
        money -= 20
        place_gun(( 
            ( x + 0.5 ) * 64, 
            ( y + 0.5 ) * 64
        ))

def place_gun(pos):
    gun = Gun(pos)
    guns.append(gun)
    actors.append(gun)

    x = math.floor(pos[0] / 64)
    y = math.floor(pos[1] / 64)
    key = "{},{}".format(x, y)

    gun_map[key] = gun

def spawn_creep():
    global creep_spawn_point
    creep = Creep(creep_spawn_point, level * 100)
    creeps.append(creep)
    actors.append(creep)

def spawn_creeps(amount, delay):
    for i in range(amount):
        clock.schedule(spawn_creep, (delay * i) / 1000.0)


# spawn_creeps(20, 1000)